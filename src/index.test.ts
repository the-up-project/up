import path from "path";

/**
 * Mock the filesystem accesses
 * use `vol.fromNestedJSON(...)` to set a new filesystem
 */
import { Volume } from "memfs";

/**
 * Mock `process.cwd()`
 * set `mockCwd` to control response
 */
let mockCwd: string = path.sep;
const realCwd = process.cwd;
beforeAll(() => (process.cwd = () => mockCwd));
afterAll(() => (process.cwd = realCwd));

import up from "./index";
type Up = typeof up;
type UpOptions = Parameters<Up>[0];
type UpResult = ReturnType<Up>;

describe("up", () => {
  describe("API", () => {
    describe("test", () => {
      it("should error if not set, or set incorrectly", async () => {
        const async_options: UpOptions[] = [
          // @ts-expect-error: These parameters are deliberately incorrect!
          { test: null },
          // @ts-expect-error: These parameters are deliberately incorrect!
          { test: undefined },
          // @ts-expect-error: These parameters are deliberately incorrect!
          {},
        ];
        const sync_options: UpOptions[] = [
          // @ts-expect-error: These parameters are deliberately incorrect!
          { sync: true, test: null },
          // @ts-expect-error: These parameters are deliberately incorrect!
          { sync: true, test: undefined },
          // @ts-expect-error: These parameters are deliberately incorrect!
          { sync: true },
        ];

        expect.assertions(sync_options.length + async_options.length);

        const expectedError = new Error("A test must be specified");

        for (let i = 0; i < async_options.length; i++) {
          let options = async_options[i];
          expect(up(options)).rejects.toMatchObject(expectedError);
        }
        for (let i = 0; i < sync_options.length; i++) {
          let options = sync_options[i];
          expect(() => up(options)).toThrow(expectedError);
        }
      });

      it("should not error if given valid tests", async () => {
        expect(() => up({ test: "data" })).not.toThrow();
        expect(() => up({ test: () => true, sync: true })).not.toThrow();
        expect(() => up({ test: /a/ })).not.toThrow();
        expect(() => up({ test: async () => true })).not.toThrow();
      });
    });
  });

  const json = {
    "/example": {
      filesystem: {
        with: {
          data: "42",
        },
      },
      data: {
        in: {
          system: "7",
        },
      },
    },
  };

  const vol = Volume.fromNestedJSON(json);

  function makePath(...parts: Array<string>): string {
    return path.sep + path.join(...parts);
  }

  describe("use cases", () => {
    beforeEach(async () => {
      mockCwd = makePath("example", "filesystem", "with");
    });

    (
      [
        {
          name: "sync",
          sync: true,
          fs: vol,
        },
        {
          name: "sync - restricted",
          sync: true,
          fs: { readdirSync: (path: string) => vol.readdirSync(path) },
        },
        {
          name: "async",
          sync: false,
          fs: vol,
        },
        {
          name: "async - restricted readdir as Promise",
          sync: false,
          fs: { readdir: (path: string) => vol.promises.readdir(path) },
        },
        {
          name: "async - restricted readdir as callback",
          sync: false,
          fs: {
            readdir: (
              path: string,
              callback: (e: Error | undefined | null, data: string[]) => void,
            ) =>
              // @ts-expect-error: mem-fs doesn't get the types quite right here.
              vol.readdir(path, callback),
          },
        },
        {
          name: "async - restricted promises.readdir as Promise",
          sync: false,
          fs: {
            promises: { readdir: (path: string) => vol.promises.readdir(path) },
          },
        },
      ] as const
    ).forEach(({ name, sync, fs }) => {
      describe(name, () => {
        const tests: {
          name: string;
          options: UpOptions;
          result: UpResult;
        }[] = [
          {
            name: "regex",
            options: {
              test: /d/,
            },
            result: makePath("example", "filesystem", "with"),
          },
          {
            name: "recursive regex",
            options: { test: /d/, recurse: true },
            result: [
              makePath("example", "filesystem", "with"),
              makePath("example"),
            ],
          },
          {
            name: "inverted regex",
            options: {
              test: /d/,
              invert: true,
            },
            result: makePath("example", "filesystem"),
          },
          {
            name: "inverted recursive regex",
            options: {
              test: /d/,
              invert: true,
              recurse: true,
            },
            result: [makePath("example", "filesystem"), path.sep],
          },
        ];

        for (const test of tests) {
          it(test.name, async () => {
            const options = Object.assign({ sync, fs }, test.options);
            if (options.sync) {
              expect(up(options)).toEqual(test.result);
            } else {
              await expect(up(options)).resolves.toEqual(test.result);
            }
          });
        }
      });
    });
  });
});
