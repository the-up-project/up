/**
 * Up - match parental directories
 *
 * Author: Eoin Groat <up /-\T normal-gaussian.com>
 *           __  ___________
 *          /""\("     _   ")
 *         /    \)__/  \\__/
 *        /' /\  \  \\_ /
 *   up  //  __'  \ |.  | normal-gaussian.com
 *      /   /  \\  \\:  |
 *     (___/    \___)\__|
 *
 * Copyright: Eoin Groat, 2020
 *
 * LICENSE: BSD-3-Clause license
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the author nor the names of any contributors
 *    may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR "AS IS" AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */
import systemFS from "fs";
import path from "path";

type Dirent = {
  name: string;
  isDirectory(): boolean;
};

type ReaddirSync = {
  (path: string, options: { withFileTypes: true }): Dirent[];
  (path: string): string[];
};
type ReaddirCallback = {
  (path: string, cb: (err: Error | null, files: string[]) => void): void;
  (
    path: string,
    options: { withFileTypes: true },
    cb: (err: Error | null, files: Dirent[]) => void,
  ): void;
};
type ReaddirPromise = {
  (path: string): Promise<string[]>;
  (path: string, options: { withFileTypes: true }): Promise<Dirent[]>;
};

type FSReaddirSync = { readdirSync: ReaddirSync };
type FSReaddirAsync = {
  readdir?: ReaddirPromise | ReaddirCallback;
  promises?: { readdir: ReaddirPromise };
};

function getAsyncReaddir(fs: FSReaddirAsync): ReaddirPromise {
  if (fs.promises) {
    return fs.promises.readdir;
  } else if (!fs.readdir) {
    throw new Error(`Some form of async readdir function is required on 'fs'`);
  }
  const r = fs.readdir;

  // @ts-expect-error: typescript thinks the function outputs Promise<string[] | Dirent[]> not Promise<string[]> | Promise<Dirent[]>
  const readdir: ReaddirPromise = function (...args) {
    let pass = (_: Dirent[]) => {};
    let fail = (_: unknown) => {};
    const promise = new Promise<Dirent[]>((p, f) => {
      pass = p;
      fail = f;
    });

    try {
      // @ts-expect-error: typescript can't work out the arg type on the input fn
      const result = r(...args, (err, files) => {
        err ? fail(err) : pass(files);
      });
      return result ? result : promise;
    } catch (e) {
      fail(e);
      return promise;
    }
  };
  return readdir;
}

type SyncRecursiveSettings = {
  test: ((path: string) => boolean) | string | RegExp;
  sync: true;
  fs?: FSReaddirSync;
  recurse: true;
  directory?: string;
  invert?: boolean;
  direction?: "up" | "down";
};

type AsyncRecursiveSettings = {
  test: ((path: string) => Promise<boolean>) | string | RegExp;
  sync?: false;
  fs?: FSReaddirAsync;
  recurse: true;
  directory?: string;
  invert?: boolean;
  direction?: "up" | "down";
};

type SyncNonRecursiveSettings = {
  test: ((path: string) => boolean) | string | RegExp;
  sync: true;
  fs?: FSReaddirSync;
  recurse?: false;
  directory?: string;
  invert?: boolean;
  direction?: "up" | "down";
};

type AsyncNonRecursiveSettings = {
  test: ((path: string) => Promise<boolean>) | string | RegExp;
  sync?: false;
  fs?: FSReaddirAsync;
  recurse?: false;
  directory?: string;
  invert?: boolean;
  direction?: "up" | "down";
};

/**
 * @ignore
 */
function regexTest(
  fs: FSReaddirSync,
  regex: RegExp,
): (path: string) => boolean {
  return function test(path: string): boolean {
    const files = fs.readdirSync(path);
    return files.some((file) => regex.test(file));
  };
}

/**
 * @ignore
 */
function asyncRegexTest(
  fs: FSReaddirAsync,
  regex: RegExp,
): (path: string) => Promise<boolean> {
  const readdir = getAsyncReaddir(fs);
  return async function test(path: string): Promise<boolean> {
    const files = await readdir(path);
    const result = files.some((file) => regex.test(file));
    return result;
  };
}

/**
 * @ignore
 */
function stringTest(
  fs: FSReaddirSync,
  filename: string,
): (path: string) => boolean {
  return function test(path: string): boolean {
    const files = fs.readdirSync(path);
    return files.indexOf(filename) >= 0;
  };
}

/**
 * @ignore
 */
function asyncStringTest(
  fs: FSReaddirAsync,
  filename: string,
): (path: string) => Promise<boolean> {
  const readdir = getAsyncReaddir(fs);
  return async function test(path: string): Promise<boolean> {
    const files = await readdir(path);
    return files.indexOf(filename) >= 0;
  };
}

/**
 * @ignore
 */
function invertTest(
  test: (path: string) => boolean,
): (path: string) => boolean {
  return (path: string) => !test(path);
}

/**
 * @ignore
 */
function asyncInvertTest(
  test: (path: string) => Promise<boolean>,
): (path: string) => Promise<boolean> {
  return async (path: string) => !(await test(path));
}

/**
 * @ignore
 */
function sync(
  direction: "up" | "down",
  settings: SyncRecursiveSettings | SyncNonRecursiveSettings,
): null | string | string[] {
  let { directory, test, fs: userFS, recurse, invert } = settings;

  if (!test) {
    throw new Error("A test must be specified");
  }

  const fs = userFS || systemFS;
  recurse = Boolean(recurse);
  invert = Boolean(invert);
  direction = direction ?? "up";

  if (!directory) {
    directory = process.cwd();
  } else if (!path.isAbsolute(directory)) {
    directory = path.resolve(directory);
  }

  if (test instanceof RegExp) {
    test = regexTest(fs, test);
  } else if (typeof test === "string") {
    test = stringTest(fs, test);
  }

  if (invert) {
    test = invertTest(test);
  }

  function readdir(path: string): Dirent[] {
    if (userFS) {
      return userFS.readdirSync(path, { withFileTypes: true });
    } else {
      return systemFS.readdirSync(path, { withFileTypes: true });
    }
  }

  if (direction === "up") {
    const result = [];

    let lastdir = null;
    let current_directory = directory;
    while (lastdir != current_directory) {
      if (test(current_directory)) {
        if (!recurse) {
          return current_directory;
        }
        result.push(current_directory);
      }
      lastdir = current_directory;
      current_directory = path.dirname(current_directory);
    }
    if (recurse) {
      return result;
    }
    return null;
  } else {
    const result = [];
    const unvisited = [directory];
    let dir: string | undefined;
    while ((dir = unvisited.shift())) {
      let d: string = dir;

      if (test(d)) {
        if (!recurse) {
          return d;
        }
        result.push(d);
      }
      unvisited.push(
        ...readdir(d)
          .filter((f) => f.isDirectory())
          .map((f) => path.join(d, f.name)),
      );
    }
    if (recurse) {
      return result;
    }
    return null;
  }
}

/**
 * @ignore
 */
function async(
  direction: "up" | "down",
  settings: AsyncRecursiveSettings | AsyncNonRecursiveSettings,
): Promise<string[]> | Promise<string | null> {
  // This cannot be done inside _asyncUp as an `async` function must return `Promise<A | B>` and not `Promise<A> | Promise<B>`
  const r1: Promise<string[] | string | null> = _asyncUp(direction, settings);
  // @ts-expect-error: /we/ know that the recurse option differentiates these two outputs, but typescript does not.
  const r2: Promise<string[]> | Promise<string | null> = r1;
  return r2;
}
async function _asyncUp(
  direction: "up" | "down",
  settings: AsyncRecursiveSettings | AsyncNonRecursiveSettings,
): Promise<string[] | string | null> {
  let { directory, test, fs: userFS, recurse, invert } = settings;

  if (!test) {
    throw new Error("A test must be specified");
  }

  recurse = Boolean(recurse);
  invert = Boolean(invert);
  direction = direction ?? "up";

  const fs = userFS ?? systemFS;

  if (!directory) {
    directory = process.cwd();
  } else if (!path.isAbsolute(directory)) {
    directory = path.resolve(directory);
  }

  if (test instanceof RegExp) {
    test = asyncRegexTest(fs, test);
  } else if (typeof test === "string") {
    test = asyncStringTest(fs, test);
  }

  if (invert) {
    test = asyncInvertTest(test);
  }

  if (direction === "up") {
    const result: Array<string> = [];

    let lastdir = null;
    let current_directory = directory;
    while (lastdir != current_directory) {
      if (await test(current_directory)) {
        if (!recurse) {
          return current_directory;
        }
        result.push(current_directory);
      }
      lastdir = current_directory;
      current_directory = path.dirname(current_directory);
    }
    if (recurse) {
      return result;
    }
    return null;
  } else {
    const readdir = getAsyncReaddir(fs);

    const result = [];
    const unvisited = [directory];
    let dir: string | undefined;
    while ((dir = unvisited.shift())) {
      let d: string = dir;

      if (await test(d)) {
        if (!recurse) {
          return d;
        }
        result.push(d);
      }
      unvisited.push(
        ...(await readdir(d, { withFileTypes: true }))
          .filter((f) => f.isDirectory())
          .map((f) => path.join(d, f.name)),
      );
    }
    if (recurse) {
      return result;
    }
    return null;
  }
}

function oopdoonFactory(defaultDirection: "up" | "down") {
  function oopdoon(settings: SyncNonRecursiveSettings): null | string;
  function oopdoon(settings: AsyncNonRecursiveSettings): Promise<null | string>;
  function oopdoon(settings: SyncRecursiveSettings): string[];
  function oopdoon(settings: AsyncRecursiveSettings): Promise<string[]>;
  function oopdoon(
    settings:
      | SyncNonRecursiveSettings
      | AsyncNonRecursiveSettings
      | SyncRecursiveSettings
      | AsyncRecursiveSettings,
  ): Promise<string[]> | Promise<null | string> | string | null | string[];
  function oopdoon(
    settings:
      | SyncNonRecursiveSettings
      | AsyncNonRecursiveSettings
      | SyncRecursiveSettings
      | AsyncRecursiveSettings,
  ): Promise<string[]> | Promise<null | string> | string | null | string[] {
    const direction = settings.direction ?? defaultDirection;
    if (settings.sync === true) {
      return sync(direction, settings);
    } else {
      return async(direction, settings);
    }
  }
  Object.defineProperty(oopdoon, "name", {
    value: defaultDirection,
    enumerable: false,
  });
  return oopdoon;
}

/**
 * Given a directory, returns a list of parent directories that match the
 *  condition 'test'.
 *
 * Test can be either the name of a file in the directory, a regex matching the
 *  name of a file or directory in the directory, or a function that takes a
 *  directory and returns a boolean, true for a match.
 *
 * Options include
 *
 * @param options.sync Whether or not to act synchronously, returning an Error or list of matches, or asynchronously returning a Promise. Default false.
 * @param options.test The test to carry out on the directory. It can be a regex, a string that indicate a pattern or value to match in the filenames of a directories files, or a custom function returning boolean or Promise<boolean>. Required.
 * @param options.recurse Return all matching directories up to the root of the filesystem. Default false.
 * @param options.invert Return the directories that do not match instead. Default false.
 * @param options.directory The directory to start from. Defaults to the processes current working directory.
 * @param options.fs A fs object, or more precisely a fs skeleton providing the correct readdir methods for the sync/async mode of operation. Defaults to the node system package 'fs'.
 * @returns If sync is false or unset, returns a promise resolving to a list of matches. If sync is set, returns an Error or a list of matches.
 */
export const up = oopdoonFactory("up");
export const down = oopdoonFactory("down");

export default up;
