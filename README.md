# up

Search upwards in the filesystem for directories that match the "test", typically a test based on filenames. Think of unix `find`, but going upwards.

# Installation

`yarn add -D @normed/up`

# Usage

```ts
import up from '@normed/up';
up(options: {
  // Basic options
  
  /**
   * The directory to start from.
   * 
   * Defaults to the processes current working directory.
   */
  directory: string,

  /**
   * The test to carry out on the directory.
   * 
   * It can be a regex, a string that indicate a pattern or value to match in
   * the filenames of a directories files, or a custom function returning
   * boolean or Promise<boolean>.
   */
  test: string | regex | ((filename: string) => boolean | Promise<boolean>),

  // Intermediate options

  /**
   * Act as a sync or async function.
   * 
   * As an asynchronous function it has the signature:
   * `up(options: Options): Promise<string[]>`
   * 
   * As a synchronous function it has the signature:
   * `up(options: Options): string[]`
   * 
   * Defaults to false
   */
  sync?: boolean,

  /**
   * Whether to stop at the first match
   *
   * Up traverses upwards; selecting the first (closest) matching directory.
   * `recurse` will find all matching directoryies up to the root of the
   * filesystem.
   * 
   * Default false
   */
  recurse?: boolean,

  /**
   * Invert the test
   * 
   * Return the directory (or directories) that do not match the test instead.
   * 
   * Default false
   */
  invert?: boolean,
  
  // Advanced options
  
  /**
   * A 'fs' compatible object, or more precisely a fs skeleton providing the correct readdir methods for the sync/async mode of operation.
   * 
   * For sync operation this function is required:
   *  * `readdirSync(path: string): string[]`
   * 
   * For async operation, any one of these functions are required
   *  * `readdir(path: string, cb: (err: Error | null, files: string[]) => void) => void`
   *  * `readdir(path: string): Promise<string[]>`
   *  * `{ promises: readdir(path: string): Promise<string[]> }`
   * 
   * Defaults to the node system package 'fs'.
   */
  fs: typeof fs

}): Promise<Array<string>> | Array<string>
```

## Synchronous example

```ts
import up from '@normed/up';

const gitignores = up({
  sync: true,
  test: '.gitignore',
  recurse: true
});

console.log(gitignores);
// [ "/home/normed/@normed/up/.gitignore", "/home/normed/.gitinore" ]
```

## Asynchronous example

```ts
import up from '@normed/up';

const gitignores = await up({
  test: '.gitignore',
  recurse: true
});

console.log(gitignores);
// [ "/home/normed/@normed/up/.gitignore", "/home/normed/.gitinore" ]
```
