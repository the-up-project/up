import { run } from './run'

// Some assorted random snapshots, quick and easy "back-testing"
test("assorted snapshots", async () => {
  expect.assertions(4);
  await expect(run("./up", ["-f", ".up", "pwd"])).resolves.toMatchSnapshot();
  await expect(
    run("./up", ["-d", "-f", ".up", "pwd"]),
  ).resolves.toMatchSnapshot();
  await expect(
    run("./up", ["-d", "-r", "-f", ".up", "pwd"]),
  ).resolves.toMatchSnapshot();
  await expect(run("./up", ["up"])).resolves.toMatchSnapshot();
});
