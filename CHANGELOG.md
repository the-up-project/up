This package uses Semver.

Given a version number MAJOR.MINOR.PATCH, increment the:

1. MAJOR version when you make incompatible API changes,
2. MINOR version when you add functionality in a backwards compatible manner, and
3. PATCH version when you make backwards compatible bug fixes.

When changes are made, log the changes showing the from and to versions below

# 1.2.0

* Improves `README.md` by reformatting the existing documentation
* Adds an asynchronous example to `README.md`
* Supports asynchronous `readdir` through more than just `fs.promises.readdir`. Now supports a direct import of `fs/promises` and a callback only implementation.

# 1.0.0 -> 1.1.0

Corrects derivation of parent path to use the path module only, and not assume unix style filepaths. Fixes issue on Windows where the root directory always appeared in results.