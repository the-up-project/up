import { SpawnOptions, spawn } from 'child_process';
import path from 'path';

const HOME = process.env.HOME ?? '';
const HOME_regexp = new RegExp(escapeRegExp(HOME), "g");
const ROOT = path.resolve(path.join(__dirname, ".."));
const ROOT_regexp = new RegExp(escapeRegExp(ROOT), "g");

// Utility function for `addToData` taken from: https://stackoverflow.com/questions/3446170/escape-string-for-use-in-javascript-regex
function escapeRegExp(string: string) {
  return string.replace(/[.*+?^${}()|[\]\\]/g, "\\$&"); // $& means the whole matched string
}
// Utility functions to replace contents of strings:
const replaceHOME = (s: string) =>
  s.replace(HOME_regexp, "<HOME>");
const replaceROOT = (s: string) =>
  s.replace(
    ROOT_regexp,
    "<ROOT>",
  );

// Utility function for `run`
function addToData(data: {type: 'stderr' | 'stdout', data: string }[], type: 'stderr' | 'stdout') {
  return function (chunk: string) {
    chunk = chunk.toString();
    let lastPosition = data.length - 1;
    if (data.length && data[lastPosition].type === type) {
      let datum = data[lastPosition].data;
      datum += chunk;
      datum = replaceROOT(replaceHOME(datum));
      data[lastPosition].data = datum;
    } else {
      data[data.length] = {
        type: type,
        data: chunk,
      };
    }
  };
}

/* run(command, args, options): Promise<{code, signal, data}, {error, data}>
 * - params follow the form of spawn (see https://nodejs.org/api/child_process.html#child_process_child_process_spawn_command_args_options)
 * - on resolve provides an object with fields:
 *    + code: ?number,
 *    + signal: ?string,
 *    + data: Array<{type: 'stderr' | 'stdout', data: string}>
 * - on reject provides an object with fields:
 *    + error: ?Error
 *    + data: Array<{type: 'stderr' | 'stdout', data: string}>
 */
export function run(command: string, args: string[], options: SpawnOptions) {
  return new Promise((pass, fail) => {
    const child = spawn(command, args, options);

    // Time ordered output of the stderr and stdout of the program, marked up with
    // which pipe. Maintains difference between pipes whilst maintaining relationship
    // in output time.
    const data: {type: 'stderr' | 'stdout', data: string}[] = [];

    child.on("close", (code, signal) => pass({ code, signal, data }));
    child.on("error", (error) => fail({ error, data }));
    for(const pipeName of (['stderr', 'stdout'] as const)) {
      const pipe = child[pipeName];
      if(!pipe) {
        continue;
      } 
      pipe.on('data', addToData(data, pipeName));
    }
  });
};
